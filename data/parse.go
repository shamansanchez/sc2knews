package data

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"

	"gitlab.com/shamansanchez/sc2knews/decompress"
)

type ArticleData struct {
	CityName  string
	MayorName string
	TeamName  string
	Tokens    [][]string
}

func parseGroups(data map[uint32]dataSegment) ([][]uint16, error) {
	groups := [][]uint16{}

	startReader := bytes.NewReader(data[GROUP_START].Data)
	countReader := bytes.NewReader(data[GROUP_COUNT].Data)

	var start uint16
	var count uint16

	for err := binary.Read(startReader, binary.BigEndian, &start); err == nil; err = binary.Read(startReader, binary.BigEndian, &start) {
		binary.Read(countReader, binary.BigEndian, &count)

		currGroup := []uint16{}

		for i := uint16(0); i < count; i++ {
			currGroup = append(currGroup, start+i)
		}

		groups = append(groups, currGroup)
	}

	return groups, nil
}

func parseTokens(data map[uint32]dataSegment, groups [][]uint16) ([][]string, error) {
	tokens := map[uint16]string{}
	ret := [][]string{}

	pointerReader := bytes.NewReader(data[TOKEN_POINTER].Data)
	dataReader := bytes.NewReader(data[TOKEN_DATA].Data)

	var pointer uint32
	var index uint16

	for err := binary.Read(pointerReader, binary.BigEndian, &pointer); err == nil; err = binary.Read(pointerReader, binary.BigEndian, &pointer) {
		dataReader.Seek(int64(pointer), io.SeekStart)

		buf := []byte{}
		tokenBuffer := bytes.NewBuffer(buf)

		for b, err := dataReader.ReadByte(); b != 0 && err == nil; b, err = dataReader.ReadByte() {
			switch b {

			case '\\':
				next, err := dataReader.ReadByte()
				orDie(err)
				fmt.Fprintf(tokenBuffer, "%s", decompress.MapChar(next))

			case '-':
				fmt.Fprintf(tokenBuffer, "{NEWLINE}")

			case '[', '*', '^', '@', '&':
				next, err := dataReader.ReadByte()
				orDie(err)
				fmt.Fprintf(tokenBuffer, "{%c0x%X}", b, next)

			default:
				fmt.Fprintf(tokenBuffer, "%s", decompress.MapChar(b))
			}
		}

		tokens[index] = tokenBuffer.String()
		index++
	}

	for i := 0; i < len(groups); i++ {
		currGroup := []string{}

		for _, token := range groups[i] {
			currGroup = append(currGroup, tokens[token])
		}

		ret = append(ret, currGroup)
	}

	return ret, nil
}

func GetData(indexFile string, dataFile string) (ArticleData, error) {
	data := readDataSegments(indexFile, dataFile)

	groups, err := parseGroups(data)
	if err != nil {
		return ArticleData{}, err
	}

	tokens, err := parseTokens(data, groups)
	if err != nil {
		return ArticleData{}, err
	}

	articleData := ArticleData{
		MayorName: "Jason",
		CityName:  "Jasonia",
		TeamName:  "Llamas",
		Tokens:    tokens,
	}

	return articleData, nil
}
