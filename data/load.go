package data

import (
	"bytes"
	"encoding/binary"
	"log"
	"os"
)

const GROUP_START = 1000
const GROUP_COUNT = 1001
const TOKEN_POINTER = 1002
const TOKEN_DATA = 1003
const STORY_POWER = 1004
const STORY_DECAY = 1005

type dataSegment struct {
	start uint32
	size  uint32
	Data  []byte
}

func orDie(err error) {
	if err != nil {
		log.Fatal("ERROR: " + err.Error())
	}
}

func readDataSegments(indexFile string, dataFile string) map[uint32]dataSegment {
	starts := map[uint32]uint32{}
	dataSegments := map[uint32]dataSegment{}

	indexBytes, err := os.ReadFile(indexFile)
	orDie(err)
	indexReader := bytes.NewReader(indexBytes)

	dataBytes, err := os.ReadFile(dataFile)
	orDie(err)
	dataReader := bytes.NewReader(dataBytes)

	var id uint32
	var start uint32

	for err := binary.Read(indexReader, binary.LittleEndian, &id); err == nil; err = binary.Read(indexReader, binary.LittleEndian, &id) {
		err = binary.Read(indexReader, binary.LittleEndian, &start)
		starts[id] = start

		if id > 1010 {
			panic("ID probably shouldn't be this large. Did you pass in DATA_USA.IDX?")
		}
	}

	for id, currStart := range starts {
		nextStart, ok := starts[id+1]

		if !ok {
			nextStart = uint32(len(dataBytes))
		}

		size := nextStart - currStart

		data := make([]byte, size)
		dataReader.ReadAt(data, int64(currStart))

		dataSegments[id] = dataSegment{
			start: currStart,
			size:  size,
			Data:  data,
		}
	}

	return dataSegments
}
