package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"gitlab.com/shamansanchez/sc2knews/data"
	"gitlab.com/shamansanchez/sc2knews/news"
)

func orDie(err error) {
	if err != nil {
		log.Fatal("ERROR: " + err.Error())
	}
}

func printAll(articleData data.ArticleData) {
	for groupId := 0; groupId < len(articleData.Tokens); groupId++ {
		fmt.Printf("===0x%X\n", groupId)
		for tokenId, token := range articleData.Tokens[groupId] {
			fmt.Printf("%d :: %s\n", tokenId, token)
		}
		fmt.Println()
	}
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	all := flag.Bool("all", false, "all")
	jsonFile := flag.String("json", "articledata.json", "JSON file from parsedat")
	groupId := flag.Int("group", -1, "Group ID")
	tokenId := flag.Int("token", -1, "Token ID")
	flag.Parse()

	tokenJSON, err := os.ReadFile(*jsonFile)
	orDie(err)

	articleData := data.ArticleData{}
	json.Unmarshal(tokenJSON, &articleData)

	if *all {
		printAll(articleData)
		return
	}

	if *groupId == -1 {
		*groupId = rand.Intn(0x43)
	}

	if *tokenId == -1 {
		*tokenId = rand.Intn(len(articleData.Tokens[*groupId]))
	}

	article, err := news.GenerateArticle(articleData, *groupId, *tokenId)
	orDie(err)

	fmt.Printf("%s\n\n", article.Headline)
	for _, line := range article.Body {
		fmt.Printf("  %s\n", line)
	}
}
