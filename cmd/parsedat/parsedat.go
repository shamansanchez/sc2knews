package main

import (
	"encoding/json"
	"flag"
	"log"
	"os"

	"gitlab.com/shamansanchez/sc2knews/data"
)

func orDie(err error) {
	if err != nil {
		log.Fatal("ERROR: " + err.Error())
	}
}

func main() {
	indexFile := flag.String("index", "DATA_USA.IDX", "index file")
	dataFile := flag.String("data", "DATA_USA.DAT", "data file")
	flag.Parse()

	tokens, err := data.GetData(*indexFile, *dataFile)
	orDie(err)

	enc := json.NewEncoder(os.Stdout)
	enc.SetEscapeHTML(false)
	enc.SetIndent("", "  ")

	enc.Encode(tokens)
}
