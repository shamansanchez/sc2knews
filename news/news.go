package news

import (
	"encoding/hex"
	"fmt"
	"math/rand"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/shamansanchez/sc2knews/data"
)

type Article struct {
	Headline string
	Body     []string
}

func replaceNames(line string, articleData *data.ArticleData) string {
	line = strings.Replace(line, "{MAYORNAME}", articleData.MayorName, -1)
	line = strings.Replace(line, "{CITYNAME}", articleData.CityName, -1)
	line = strings.Replace(line, "{TEAMNAME}", articleData.TeamName, -1)

	return line
}

func GenerateArticle(articleData data.ArticleData, group int, index int) (Article, error) {
	articles := articleData.Tokens[group]
	articleText := articles[index]

	article := Article{}

	indices := map[string]int{}

	replacements := map[string]map[string]string{
		"[": {},
		"^": {},
		"@": {},
		"&": {},
		"<": {},
	}

	controlRegex := regexp.MustCompile("(\\s*){(.)0x(..)}")

	for controlRegex.MatchString(articleText) {
		matches := controlRegex.FindAllStringSubmatch(articleText, -1)

		for _, match := range matches {
			spaces := match[1]
			controlChar := match[2]
			groupStr := match[3]

			groupIdArr, err := hex.DecodeString(groupStr)
			if err != nil {
				return article, err
			}

			groupId := groupIdArr[0]

			var newToken string
			var newTokenIndex int
			var indexOk bool

			switch controlChar {

			// * seems to pick a random token every time
			case "*":
				newToken = articleData.Tokens[groupId][rand.Intn(len(articleData.Tokens[groupId]))]

			// [ and @ pick the same token each time
			case "[", "@":
				_, ok := replacements[controlChar][groupStr]

				if !ok {
					newTokenIndex = rand.Intn(len(articleData.Tokens[groupId]))
					replacements[controlChar][groupStr] = articleData.Tokens[groupId][newTokenIndex]
				}
				newToken = replacements[controlChar][groupStr]

			// & and possibly ^ always use the same token index, even with different groups
			case "&", "^":
				_, ok := replacements[controlChar][groupStr]

				if !ok {
					newTokenIndex, indexOk = indices[controlChar]
					if !indexOk {
						newTokenIndex = rand.Intn(len(articleData.Tokens[groupId]))
						indices[controlChar] = newTokenIndex
					}
					replacements[controlChar][groupStr] = articleData.Tokens[groupId][newTokenIndex]
				}
				newToken = replacements[controlChar][groupStr]

			}

			// Words preceded by 2 spaces seem to get capitalized
			if len(spaces) == 2 {
				newToken = strings.Title(newToken)
				spaces = " "
			}

			articleText = strings.Replace(articleText, match[0], spaces+newToken, 1)
		}
	}

	// handle numbers
	numRegex := regexp.MustCompile("([<%])(\\d+)")
	numMatches := numRegex.FindAllStringSubmatch(articleText, -1)

	for _, match := range numMatches {
		// fmt.Println(match)
		controlChar := match[1]
		numStr := match[2]

		var newNum string

		switch controlChar {
		case "<":
			_, ok := replacements[controlChar][numStr]

			if !ok {
				num, err := strconv.Atoi(numStr)
				if err != nil {
					return article, err
				}
				replacements[controlChar][numStr] = fmt.Sprintf("%d", rand.Intn(num-1)+1)
			}
			newNum = replacements[controlChar][numStr]

		case "%":
			num, err := strconv.Atoi(numStr)
			if err != nil {
				return article, err
			}

			newNum = fmt.Sprintf("%d", rand.Intn(num-1)+1)
		}

		articleText = strings.Replace(articleText, match[0], newNum, 1)
	}

	// The headline of an article always ends with a +
	headlineRegex := regexp.MustCompile("^(.+?)\\++")
	headlineMatch := headlineRegex.FindStringSubmatch(articleText)

	if headlineMatch != nil {
		article.Headline = strings.Title(headlineMatch[1])
		articleText = strings.Replace(articleText, headlineMatch[0], "", 1)
	}

	paraRegex := regexp.MustCompile("({NEWLINE})+(.*?\\w)")
	paraMatches := paraRegex.FindAllStringSubmatch(articleText, -1)

	for _, match := range paraMatches {
		para := fmt.Sprintf("\n%s", strings.ToTitle(match[2]))
		// fmt.Println(match[0], " -> ", para)

		articleText = strings.Replace(articleText, match[0], para, 1)
	}

	sentenceRegex := regexp.MustCompile("([\\.\\?!].*?\\w)")
	sentenceMatches := sentenceRegex.FindAllStringSubmatch(articleText, -1)

	for _, match := range sentenceMatches {
		sentence := fmt.Sprintf("%s", strings.ToTitle(match[1]))
		articleText = strings.Replace(articleText, match[0], sentence, 1)
	}

	articleText = replaceNames(articleText, &articleData)
	article.Headline = replaceNames(article.Headline, &articleData)

	lines := strings.Split(articleText, "\n")
	article.Body = []string{}

	for _, line := range lines {
		trimLine := strings.TrimSpace(line)
		if len(trimLine) > 0 {
			article.Body = append(article.Body, trimLine)
		}
	}

	return article, nil
}
